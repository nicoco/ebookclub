from .models import Book, Comment


def stats(request):
    stats = {
        'n_books': Book.objects.count(),
        'n_comments': Comment.objects.count()
    }
    return {'stats': stats}


import zipfile
import isbnlib

from xml.etree import ElementTree

from django.urls import reverse
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.files.images import ImageFile

import wikipedia; wikipedia.set_lang(settings.LANGUAGE_CODE[:2])

class EpubParsingException(Exception):
    pass

  
class Author(models.Model):
    name = models.CharField('nom', max_length=200, unique=True)

    def __str__(self):
        return self.name.title()

    def get_absolute_url(self):
        return reverse('author_detail', args=[str(self.pk)])


class Tag(models.Model):
    name = models.CharField('nom', max_length=200, unique=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('tag_detail', args=[str(self.pk)])


class Book(models.Model):
    title = models.CharField('titre', max_length=200, default='livre sans nom')
    upload_date = models.DateTimeField("date d'ajout", auto_now_add=True)
    file = models.FileField('Fichier', upload_to='books')
    uploader = models.ForeignKey(User, on_delete=models.PROTECT)

    cover = models.ImageField(upload_to='covers', blank=True, null=True)

    publication_date = models.DateField('date de publication',
                                        blank=True, null=True)
    summary = models.TextField('Résumé', blank=True)

    authors = models.ManyToManyField(Author, verbose_name='authors', blank=True)
    tags = models.ManyToManyField(Tag, blank=True)

    def __str__(self):
        return self.title.title()

    def epubjs(self):
        return settings.EPUBJS_URL + self.file.url

    def wikipedia(self):
        results = wikipedia.search('{title}'.format(title=self.title))
        if len(results) > 0:
            page = wikipedia.page(results[0])
            result = page.url
        else:
            result = ""
        return result

    def get_absolute_url(self):
        return reverse('book_detail', args=[str(self.pk)])

    def get_update_url(self):
        return reverse('book_update', args=[str(self.pk)])
      
    def is_commented_by_user(self, user):
        for comment in self.comment_set.all():
            if comment.user == user:
                return True
        return False
    
    def likers(self):
        return [c.user for c in Comment.objects.filter(book=self) if c.recommended]

    def haters(self):
        return [c.user for c in Comment.objects.filter(book=self) if not c.recommended]

    def extract_metadata_from_epub(self):

        ns = {
            'n': 'urn:oasis:names:tc:opendocument:xmlns:container',
            'pkg': 'http://www.idpf.org/2007/opf',
            'dc': 'http://purl.org/dc/elements/1.1/'
        }

        try:
            z = zipfile.ZipFile(open(self.file.path, 'rb'))
        except zipfile.BadZipFile as e:
            raise EpubParsingException(e)

        # locate OPF file
        txt = z.read('META-INF/container.xml')
        tree = ElementTree.fromstring(txt)
        cfname = tree.find('n:rootfiles/n:rootfile', namespaces=ns)

        if cfname is None:
            raise EpubParsingException("epub root file not found")

        # find metadata
        cf = z.read(cfname.attrib['full-path'])
        tree = ElementTree.fromstring(cf)
        p = tree.find('./pkg:metadata', namespaces=ns)

        # repackage the data
        self.title = getattr(p.find('dc:title', ns), 'text', '').lower()
        author_name = getattr(p.find('dc:creator', ns), 'text', '').lower()
        if author_name:
            author, created = Author.objects.get_or_create(name=author_name)
            if created:
                author.save()
            self.authors.set([author])
        # self.authors = [Author(getattr(p.find('dc:creator', ns), 'text'))]

        # # try ISBN
        # isbn = getattr(p.find('dc:identifier', ns), 'text')
        # try:
        #     isbn_res = isbnlib.meta(isbn)
        # except isbnlib._exceptions.NotValidISBNError:
        # else:
        #     self.isbn = isbn
        #     self.authors = [Author(name) for name in isbn_res['Authors']]

        # extract cover
        m = tree.find('./pkg:manifest', namespaces=ns)
        for item in m:
            if item.attrib['id'] == 'cover-id':
                cover_url = item.attrib['href']
                print(z.open('OEBPS/' + cover_url))
                cover = ImageFile(file=z.open('OEBPS/' + cover_url))
                self.cover = cover


class Comment(models.Model):
    book = models.ForeignKey(Book, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    date = models.DateTimeField("date d'ajout", auto_now_add=True)

    content = models.TextField('Commentaire')
    recommended = models.BooleanField('Je recommande')

    def get_absolute_url(self):
        return reverse('book_detail', args=[str(self.book.pk)])

    def __str__(self):
        return 'commentaire de {user} sur {book}'.format(user=self.user, book=self.book)
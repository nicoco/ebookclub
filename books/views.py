from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.shortcuts import render
from django.urls import reverse
from django.forms.models import modelform_factory

from django.contrib import messages

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

from django_select2.forms import Select2TagWidget

from .models import Book, Author, Tag, EpubParsingException, Comment
from .forms import (UploadFileForm, DateInput, Select2TagWidgetWithSpaces,
                    BookUpdateForm, CommentCreateForm)


class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = 'books/index.html'

    def get_queryset(self):
        return Book.objects.order_by('-upload_date')[:5]
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['nav_active'] = 'index'
        context['comment_list'] = Comment.objects.order_by('-date')[:5]
        return context


class BookListView(LoginRequiredMixin, generic.ListView):
    model = Book

    def get_queryset(self):
        return Book.objects.order_by('title')

    def get_context_data(self, **kwargs):
        context = super(BookListView, self).get_context_data(**kwargs)
        context['nav_active'] = 'book_list'
        return context


class BookDetailView(LoginRequiredMixin,
                     generic.edit.CreateView,
                     generic.DetailView):
    model = Book
    form_class = CommentCreateForm
    template_name = 'books/book_detail.html'

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.book = generic.DetailView.get_object(self)
        obj.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BookDetailView, self).get_context_data(**kwargs)
        context['is_commented_by_current_user'] = generic.DetailView.get_object(self).is_commented_by_user(self.request.user)
        return context


@login_required
def comment_delete(request):
    comment = Comment.objects.get(pk=request.POST['comment_id'])
    comment.delete()
    return HttpResponseRedirect(reverse('book_detail', args=[comment.book.id]))


class BookUpdateView(LoginRequiredMixin, generic.edit.UpdateView):
    model = Book
    form_class = BookUpdateForm

@login_required
def book_delete(request):
    book = Book.objects.get(pk=request.POST['book_id'])
    book.delete()
    return HttpResponseRedirect(reverse('index'))

@login_required
def book_update(request, pk):
    book = Book.objects.get(pk=pk)
    if request.method == 'POST':
        form = BookUpdateForm(request.POST, request.FILES, instance=book)
        for field, cls in (('authors', Author) ,('tags', Tag)):
            for value in form[field].value():
                lvalue = value.lower()
                instance, created = cls.objects.get_or_create(name=lvalue)
                if created:
                    print("Created", instance)
                    instance.save()
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('book_detail',
                                        args=[book.id]))
    else:
        form = BookUpdateForm(instance=book)
    return render(request, 'books/book_form.html',
                  {'form': form, 'book': book})


class AuthorListView(LoginRequiredMixin, generic.ListView):
    model = Author

    def get_queryset(self):
        # return Author.objects.all().order_by('name')
        return Author.objects.filter(book__isnull=False).order_by('name').distinct()

    def get_context_data(self, **kwargs):
        context = super(AuthorListView, self).get_context_data(**kwargs)
        context['nav_active'] = 'author_list'
        return context


class AuthorDetailView(LoginRequiredMixin, generic.DetailView):
    model = Author


class TagListView(LoginRequiredMixin, generic.ListView):
    model = Tag


class TagDetailView(LoginRequiredMixin, generic.DetailView):
    model = Tag


class UserListView(LoginRequiredMixin, generic.ListView):
    model = User


class UserDetailView(LoginRequiredMixin, generic.DetailView):
    model = User


@login_required
def upload(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            book = Book(file=request.FILES['file'],
                        uploader=request.user)
            book.save()
            try:
                book.extract_metadata_from_epub()
            except EpubParsingException as e:
                messages.error(request, "Pas réussi à lire les métadonnées du fichier : " + str(e))
            else:
                book.save()
            return HttpResponseRedirect(reverse('book_update',
                                        args=[book.id]))
    else:
        form = UploadFileForm()
    return render(request, 'books/upload.html', {'form': form, 'nav_active': 'upload'})

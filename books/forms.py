from django import forms
from django_select2.forms import Select2TagWidget, Select2TagMixin, HeavySelect2TagWidget

from .models import Book, Author, Tag, Comment

class DateInput(forms.DateInput):
    input_type = 'date'

class Select2TagWidgetWithSpaces(Select2TagWidget):
    def build_attrs(self, *args, **kwargs):
        """Add select2's tag attributes."""
        self.attrs.setdefault('data-minimum-input-length', 1)
        self.attrs.setdefault('data-tags', 'true')
        self.attrs.setdefault('data-token-separators', '[","]')
        return super(Select2TagMixin, self).build_attrs(*args, **kwargs)


class UploadFileForm(forms.Form):
    file = forms.FileField()


class BookUpdateForm(forms.ModelForm):
    class Meta:
        model = Book
        fields=('title', 'summary', 'publication_date', 'authors', 'tags', 'cover')
        widgets={'publication_date': DateInput,
                 'authors': Select2TagWidgetWithSpaces,
                 'tags': Select2TagWidget}

    authors = forms.ModelMultipleChoiceField(queryset=Author.objects.all(),
                                             to_field_name='name',
                                             widget=Select2TagWidgetWithSpaces,
                                             required=False)
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          to_field_name='name',
                                          widget=Select2TagWidget,
                                          required=False)


class CommentCreateForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('content', 'recommended')

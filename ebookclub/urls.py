from django.contrib import admin
from django.urls import include, path, reverse
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', include('books.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('select2/', include('django_select2.urls')),
]

if settings.DEBUG:
   urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from urllib.request import (build_opener,
                            HTTPPasswordMgrWithDefaultRealm,
                            HTTPBasicAuthHandler,
                            HTTPError)

from django.contrib.auth.models import User

from django.conf import settings


class ProsodyAuthenticationBackend:
    def authenticate(self, request, username=None, password=None):
        login_valid = prosody_auth(username, password)
        username = username.split('@')[0]
        if login_valid:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                user = User(username=username)
                user.is_staff = True
                user.is_superuser = False
                user.save()
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

def prosody_auth(username, password):
    password_mgr = HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None,
                                uri=settings.PROSODY_AUTH,
                                user=username,
                                passwd=password)
    handler = HTTPBasicAuthHandler(password_mgr)
    opener = build_opener(handler)
    try:
        opener.open(settings.PROSODY_AUTH)
    except HTTPError as e:
        return False
    else:
        return True
